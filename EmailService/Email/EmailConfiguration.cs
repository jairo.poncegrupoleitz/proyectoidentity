﻿namespace Fiit.EmailService.Email
{
    public class EmailConfiguration : IEmailConfiguration
    {
        public string SmtpServer { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpUsername { get; set; }

        public string SmtpPassword { get; set; }

        public string Identifier { get; set; }

        public bool UseSSL { get; set; }

        public bool SkipCertificateValidation { get; set; }
    }
}
