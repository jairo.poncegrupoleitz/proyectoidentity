﻿namespace Fiit.EmailService.Email
{
    public class EmailAttachment
    {
        public string FileName { get; set; }

        public byte[] Data { get; set; }
    }
}
