﻿using Fiit.EmailService.Outbound;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Http;
using MimeKit;
using System.Security.Claims;

namespace Fiit.EmailService.Email.Outbound
{
    public class EmailService : IEmailService<EmailMessage>
    {
        private readonly IEmailConfiguration emailConfiguration;
        private readonly IHttpContextAccessor contextAccessor;

        public EmailService(
            IEmailConfiguration emailConfiguration,
            IHttpContextAccessor contextAccessor)
        {
            this.emailConfiguration = emailConfiguration;
            this.contextAccessor = contextAccessor;
        }

        public async Task SendEmailAsync(EmailMessage message)
        {
            var mail = this.CreateMail(message);

            var user = contextAccessor.HttpContext.User;

            var user2 = Thread.CurrentPrincipal?.Identity?.Name;

            List<MailboxAddress> addressList = new List<MailboxAddress>();
            if (message.To != null && message.To.Any())
            {
                addressList = message.To.Select(x =>
                {
                    if (string.IsNullOrWhiteSpace(x.Name))
                    {
                        return new MailboxAddress(null, x.Address);
                    }
                    else
                    {
                        return new MailboxAddress(x.Name, x.Address);
                    }
                }).ToList();
            }

            List<MailboxAddress> ccAddressList = new List<MailboxAddress>();
            if (message.CarbonCopy != null && message.CarbonCopy.Any())
            {
                ccAddressList.AddRange(message.CarbonCopy.Select(x =>
                {
                    return new MailboxAddress(x.Name, x.Address);
                }).ToList());
            }

            List<MailboxAddress> bccAddressList = new List<MailboxAddress>();
            if (message.BlackCarbonCopy != null && message.BlackCarbonCopy.Any())
            {
                bccAddressList.AddRange(message.BlackCarbonCopy.Select(x =>
                {
                    return new MailboxAddress(x.Name, x.Address);
                }).ToList());
            }

            IEnumerable<MimeMessage> emails = Enumerable.Empty<MimeMessage>();
            if (message.SendIndividual)
            {
                emails = addressList.Select(a => this.CreateIndividualEmails(mail, new[] { a }, Enumerable.Empty<MailboxAddress>(), Enumerable.Empty<MailboxAddress>()))
                    .Concat(ccAddressList.Select(a => this.CreateIndividualEmails(mail, Enumerable.Empty<MailboxAddress>(), new[] { a }, Enumerable.Empty<MailboxAddress>())))
                    .Concat(bccAddressList.Select(a => this.CreateIndividualEmails(mail, Enumerable.Empty<MailboxAddress>(), Enumerable.Empty<MailboxAddress>(), new[] { a })));
            }
            else
            {
                emails = new[] { this.CreateIndividualEmails(mail, addressList, ccAddressList, bccAddressList) };
            }

            try
            {
                using var emailClient = new SmtpClient();
                if (this.emailConfiguration.SkipCertificateValidation)
                {
                    emailClient.ServerCertificateValidationCallback = (s, c, h, e) => true;
                }

                await emailClient.ConnectAsync(this.emailConfiguration.SmtpServer, this.emailConfiguration.SmtpPort, this.emailConfiguration.UseSSL);

                if (!string.IsNullOrWhiteSpace(this.emailConfiguration.SmtpUsername) || !string.IsNullOrWhiteSpace(this.emailConfiguration.SmtpPassword))
                {
                    await emailClient.AuthenticateAsync(this.emailConfiguration.SmtpUsername, this.emailConfiguration.SmtpPassword);
                }

                foreach (var mailMessage in emails)
                {
                    await emailClient.SendAsync(mailMessage);
                }

                await emailClient.DisconnectAsync(true);
            }
            catch (Exception e)
            {
            }
        }

        private MimeMessage CreateMail(EmailMessage message)
        {
            var mail = new MimeMessage();

            if (message.From != null)
            {
                if (string.IsNullOrWhiteSpace(message.From.Name))
                {
                    mail.From.Add(new MailboxAddress(null, message.From.Address));
                }
                else
                {
                    mail.From.Add(new MailboxAddress(message.From.Name, message.From.Address));
                }
            }
            else
            {
                mail.From.Add(new MailboxAddress(null, string.Empty));
            }

            if (message.ReplyTo != null)
            {
                if (string.IsNullOrWhiteSpace(message.ReplyTo.Name))
                {
                    mail.ReplyTo.Add(new MailboxAddress(null, message.ReplyTo.Address));
                }
                else
                {
                    mail.ReplyTo.Add(new MailboxAddress(message.ReplyTo.Name, message.ReplyTo.Address));
                }
            }

            if (!string.IsNullOrWhiteSpace(message.Subject))
            {
                mail.Subject = message.Subject;
            }

            var builder = new BodyBuilder();
            if (message.Body != null)
            {
                builder.TextBody = message.Body;

                if (message.IsBodyHtml)
                {
                    builder.HtmlBody = message.Body;
                }

                if (message.Attachments != null && message.Attachments.Any())
                {
                    foreach (var item in message.Attachments)
                    {
                        builder.Attachments.Add(item.FileName, item.Data);
                    }
                }
            }

            mail.Body = builder.ToMessageBody();

            return mail;
        }

        private MimeMessage CreateIndividualEmails(MimeMessage message, IEnumerable<MailboxAddress> addressList, IEnumerable<MailboxAddress> ccAddressList, IEnumerable<MailboxAddress> bccAddressList)
        {
            message.To.Clear();
            message.Cc.Clear();
            message.Bcc.Clear();

            foreach (var item in addressList)
            {
                message.To.Add(item);
            }

            foreach (var item in ccAddressList)
            {
                message.Cc.Add(item);
            }

            foreach (var item in bccAddressList)
            {
                message.Bcc.Add(item);
            }

            return message;
        }
    }
}
