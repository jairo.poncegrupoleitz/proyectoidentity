﻿using Fiit.EmailService.Outbound;

namespace Fiit.EmailService.Email.Outbound
{
    public class EmailMessage : IEmailMessage
    {
        public bool SendIndividual { get; set; }

        public EmailAddress From { get; set; }

        public IEnumerable<EmailAddress> To { get; set; }

        public IEnumerable<EmailAddress> CarbonCopy { get; set; }

        public IEnumerable<EmailAddress> BlackCarbonCopy { get; set; }

        public IEnumerable<EmailAttachment> Attachments { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public bool IsBodyHtml { get; set; }

        public EmailAddress ReplyTo { get; set; }
    }
}
