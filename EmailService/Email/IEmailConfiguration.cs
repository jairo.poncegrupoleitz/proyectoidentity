﻿namespace Fiit.EmailService.Email
{
    public interface IEmailConfiguration
    {
        string SmtpServer { get; set; }

        int SmtpPort { get; set; }

        string SmtpUsername { get; set; }

        string SmtpPassword { get; set; }

        string Identifier { get; set; }

        bool UseSSL { get; set; }

        bool SkipCertificateValidation { get; set; }
    }
}
