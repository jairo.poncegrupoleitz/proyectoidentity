﻿namespace Fiit.EmailService.Outbound
{
    public interface IEmailService<TMessage>
        where TMessage : IEmailMessage
    {
        Task SendEmailAsync(TMessage message);
    }
}
