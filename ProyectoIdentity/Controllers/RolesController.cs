﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ProyectoIdentity.Datos;

namespace ProyectoIdentity.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class RolesController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public RolesController(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var roles = _context.Roles.ToList();

            return View(roles);
        }

        [HttpGet]
        public IActionResult Crear()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Crear(IdentityRole rol)
        {
            if (await _roleManager.RoleExistsAsync(rol.Name))
            {
                TempData["Error"] = "El nombre de rol ya existe";
                return RedirectToAction(nameof(Index));
            }

            // Se crea el rol:
            await _roleManager.CreateAsync(new IdentityRole() { Name = rol.Name });
            TempData["Correcto"] = "Rol creado correctamente";

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Editar(string id)
        {
            if (String.IsNullOrEmpty(id))
            {
                return View();
            }
            else
            {
                // Actualizar el rol:
                var rolDb = _context.Roles.FirstOrDefault(r => r.Id.Equals(id));
                return View(rolDb);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Editar(IdentityRole rol)
        {
            if (await _roleManager.RoleExistsAsync(rol.Name))
            {
                TempData["Error"] = "El nombre de rol ya existe";
                return RedirectToAction(nameof(Index));
            }

            var rolDb = _context.Roles.FirstOrDefault(r => r.Id.Equals(rol.Id));
            if (rolDb is null)
            {
                return RedirectToAction(nameof(Index));
            }

            rolDb.Name = rol.Name;
            rolDb.NormalizedName = rol.Name.ToUpper();

            var resultado = await _roleManager.UpdateAsync(rolDb);
            TempData["Correcto"] = "Rol editado correctamente";

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Borrar(string id)
        {
            var rolDb = _context.Roles.FirstOrDefault(r => r.Id.Equals(id));
            if (rolDb is null)
            {
                TempData["Error"] = "No existe el rol seleccionado";
                return RedirectToAction(nameof(Index));
            }

            var usuariosParaEsteRol = _context.UserRoles.Where(u => u.RoleId == id).Count();
            if (usuariosParaEsteRol > 0)
            {
                TempData["Error"] = "El rol tiene usuarios asignados, no se puede borrar";
                return RedirectToAction(nameof(Index));
            }

            await _roleManager.DeleteAsync(rolDb);
            TempData["Correcto"] = "Rol borrado correctamente";

            return RedirectToAction(nameof(Index));
        }

    }
}
