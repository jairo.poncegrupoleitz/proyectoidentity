﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ProyectoIdentity.Models;
using System.Security.Claims;
using System.Text.Encodings.Web;

namespace ProyectoIdentity.Controllers
{
    [Authorize]
    public class CuentasController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IEmailSender _emailSender;
        public readonly UrlEncoder _urlEncoder;

        public CuentasController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IEmailSender emailSender,
            UrlEncoder urlEncoder,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _urlEncoder = urlEncoder;
            _roleManager = roleManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Registro(string? returnurl)
        {
            // Para la creacion de los roles:
            if (!await _roleManager.RoleExistsAsync("Administrador"))
            {
                // Creacion del rol usuario Administrador:
                await _roleManager.CreateAsync(new IdentityRole("Administrador"));
            }

            if (!await _roleManager.RoleExistsAsync("Registrado"))
            {
                // Creacion del rol usuario Administrador:
                await _roleManager.CreateAsync(new IdentityRole("Registrado"));
            }

            ViewData["ReturnUrl"] = returnurl;

            RegisterViewModel registro = new RegisterViewModel();
            return View(registro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Registro(RegisterViewModel registro, string? returnurl)
        {
            ViewData["ReturnUrl"] = returnurl;
            returnurl = returnurl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                var usuario = new UserApp
                {
                    UserName = registro.Email,
                    Email = registro.Email,
                    Nombre = registro.Nombre,
                    Url = registro.Url,
                    CodigoPais = registro.CodigoPais,
                    Telefono = registro.Telefono,
                    Pais = registro.Pais,
                    Ciudad = registro.Ciudad,
                    Direccion = registro.Direccion,
                    FechaNacimiento = registro.FechaNacimiento,
                    Estado = registro.Estado
                };

                var resultado = await _userManager.CreateAsync(usuario, registro.Password);

                if (resultado.Succeeded)
                {
                    // Esta linea es para la asignacion del usuario que se registra al rol "Registrado":
                    await _userManager.AddToRoleAsync(usuario, "Registrado");

                    // Implementacion de confirmacion de email en el registro:
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(usuario);
                    var urlRetorno = Url.Action("ConfirmarEmail", "Cuentas", new { userId = usuario.Id, code = code }, protocol: HttpContext.Request.Scheme);

                    await _emailSender.SendEmailAsync(registro.Email, "Confirmar cuenta - Proyecto Identity",
                    "Por favor, confirme su cuenta dando <a href='" + urlRetorno + "'>clic aquí</a>");

                    await _signInManager.SignInAsync(usuario, isPersistent: false);

                    //return RedirectToAction("Index", "Home");

                    return LocalRedirect(returnurl);
                }

                ValidarErrores(resultado);
            }

            return View(registro);
        }

        // Registro especial solo para los administradores:
        [HttpGet]
        public async Task<IActionResult> RegistroAdministrador(string? returnurl)
        {
            // Para la creacion de los roles:
            if (!await _roleManager.RoleExistsAsync("Administrador"))
            {
                // Creacion del rol usuario Administrador:
                await _roleManager.CreateAsync(new IdentityRole("Administrador"));
            }

            if (!await _roleManager.RoleExistsAsync("Registrado"))
            {
                // Creacion del rol usuario Administrador:
                await _roleManager.CreateAsync(new IdentityRole("Registrado"));
            }

            // Para seleccion de rol:
            List<SelectListItem> listaRoles = new List<SelectListItem>();
            listaRoles.Add(new SelectListItem()
            {
                Value = "Registrado",
                Text = "Registrado"
            });

            listaRoles.Add(new SelectListItem()
            {
                Value = "Administrador",
                Text = "Administrador"
            });

            ViewData["ReturnUrl"] = returnurl;

            RegisterViewModel registro = new RegisterViewModel()
            {
                ListaRoles = listaRoles
            };
            return View(registro);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RegistroAdministrador(RegisterViewModel registro, string? returnurl)
        {
            ViewData["ReturnUrl"] = returnurl;
            returnurl = returnurl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                var usuario = new UserApp
                {
                    UserName = registro.Email,
                    Email = registro.Email,
                    Nombre = registro.Nombre,
                    Url = registro.Url,
                    CodigoPais = registro.CodigoPais,
                    Telefono = registro.Telefono,
                    Pais = registro.Pais,
                    Ciudad = registro.Ciudad,
                    Direccion = registro.Direccion,
                    FechaNacimiento = registro.FechaNacimiento,
                    Estado = registro.Estado
                };

                var resultado = await _userManager.CreateAsync(usuario, registro.Password);

                if (resultado.Succeeded)
                {
                    // Para seleccion de rol en el registro:
                    if (registro.RolSeleccionado != null && registro.RolSeleccionado.Length > 0 && registro.RolSeleccionado == "Administrador")
                    {
                        await _userManager.AddToRoleAsync(usuario, "Administrador");
                    }
                    else
                    {
                        await _userManager.AddToRoleAsync(usuario, "Registrado");
                    }

                    // Esta linea es para la asignacion del usuario que se registra al rol "Registrado":
                    await _userManager.AddToRoleAsync(usuario, "Registrado");

                    // Implementacion de confirmacion de email en el registro:
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(usuario);
                    var urlRetorno = Url.Action("ConfirmarEmail", "Cuentas", new { userId = usuario.Id, code = code }, protocol: HttpContext.Request.Scheme);

                    await _emailSender.SendEmailAsync(registro.Email, "Confirmar cuenta - Proyecto Identity",
                    "Por favor, confirme su cuenta dando <a href='" + urlRetorno + "'>clic aquí</a>");

                    await _signInManager.SignInAsync(usuario, isPersistent: false);

                    //return RedirectToAction("Index", "Home");

                    return LocalRedirect(returnurl);
                }

                ValidarErrores(resultado);
            }

            // Para seleccion de rol:
            List<SelectListItem> listaRoles = new List<SelectListItem>();
            listaRoles.Add(new SelectListItem()
            {
                Value = "Registrado",
                Text = "Registrado"
            });

            listaRoles.Add(new SelectListItem()
            {
                Value = "Administrador",
                Text = "Administrador"
            });

            registro.ListaRoles = listaRoles;

            return View(registro);
        }

        // Manejador de errores:
        [AllowAnonymous]
        private void ValidarErrores(IdentityResult resultado)
        {
            foreach (var error in resultado.Errors)
            {
                ModelState.AddModelError(String.Empty, error.Description);
            }
        }

        // Metodo para mostrar formulario de acceso:
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Acceso(string? returnurl)
        {
            ViewData["ReturnUrl"] = returnurl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Acceso(AccesoViewModel acceso, string? returnurl)
        {
            ViewData["ReturnUrl"] = returnurl;
            returnurl = returnurl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                var resultado = await _signInManager.PasswordSignInAsync(acceso.Email, acceso.Password, acceso.RememberMe, lockoutOnFailure: true);

                if (resultado.Succeeded)
                {
                    //return RedirectToAction("Index", "Home");
                    return LocalRedirect(returnurl);
                }
                else if (resultado.IsLockedOut)
                {
                    return View("Bloqueado");
                }
                // Para autenticacion de dos factores:
                else if (resultado.RequiresTwoFactor)
                {
                    return RedirectToAction(nameof(EnviarCodigoEmail), new { email = acceso.Email, returnUrl = returnurl, rememberMe = acceso.RememberMe });
                }
                else
                {
                    ModelState.AddModelError(String.Empty, "Acceso inválido");
                    return View(acceso);
                }
            }

            return View(acceso);
        }

        // Salir o cerra sesion de la aplicacion:
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SalirAplicacion()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        // Metodo para olvido de contrasena:
        [HttpGet]
        [AllowAnonymous]
        public IActionResult OlvidoPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> OlvidoPassword(OlvidoPasswordViewModel olvidoPassword)
        {
            if (ModelState.IsValid)
            {
                var usuario = await _userManager.FindByEmailAsync(olvidoPassword.Email);
                if (usuario == null)
                {
                    return RedirectToAction("ConfirmacionOlvidoPassword");
                }

                var codigo = await _userManager.GeneratePasswordResetTokenAsync(usuario);
                var urlRetorno = Url.Action("ResetPassword", "Cuentas", new { userId = usuario.Id, code = codigo }, protocol: HttpContext.Request.Scheme);

                await _emailSender.SendEmailAsync(olvidoPassword.Email, "Recuperar contraseña - Proyecto Identity",
                    "Por favor, recupere su contraseña dando <a href='" + urlRetorno + "'>clic aquí</a>");

                return RedirectToAction("ConfirmacionOlvidoPassword");
            }

            return View(olvidoPassword);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ConfirmacionOlvidoPassword()
        {
            return View();
        }

        // Funcionalidad para recuperar contrasena:
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string? code)
        {
            return code == null ? View("Error") : View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(RecuperaPasswordViewModel recuperaPass)
        {
            if (ModelState.IsValid)
            {
                var usuario = await _userManager.FindByEmailAsync(recuperaPass.Email);
                if (usuario == null)
                {
                    return RedirectToAction("ConfirmacionRecuperaPassword");
                }

                var resultado = await _userManager.ResetPasswordAsync(usuario, recuperaPass.Code, recuperaPass.Password);
                if (resultado.Succeeded)
                {
                    return RedirectToAction("ConfirmacionRecuperaPassword");
                }

                ValidarErrores(resultado);
            }

            return View(recuperaPass);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ConfirmacionRecuperaPassword()
        {
            return View();
        }

        // Metodo para confirmacion de Email en el registro:
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmarEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }

            var usuario = await _userManager.FindByIdAsync(userId);
            if (usuario == null)
            {
                return View("Error");
            }

            var resultado = await _userManager.ConfirmEmailAsync(usuario, code);
            return View(resultado.Succeeded ? "ConfirmarEmail" : "Error");
        }

        // Configuracion de acceso externo (facebook, twitter, google, etc.):
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult AccesoExterno(string proveedor, string? returnurl)
        {
            var urlRedireccion = Url.Action("AccesoExternoCallback", "Cuentas", new { ReturnUrl = returnurl });
            var propiedades = _signInManager.ConfigureExternalAuthenticationProperties(proveedor, urlRedireccion);
            return Challenge(propiedades, proveedor);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> AccesoExternoCallback(string? returnurl, string? error)
        {
            returnurl = returnurl ?? Url.Content("~/");

            if (error != null)
            {
                ModelState.AddModelError(string.Empty, $"Error en el acceso externo {error}");
                return View(nameof(Acceso));
            }

            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Acceso));
            }

            // Acceder con el usuario en el proveedor externo:
            var resultado = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            if (resultado.Succeeded)
            {
                // Actualizar los tokens de acceso:
                await _signInManager.UpdateExternalAuthenticationTokensAsync(info);
                return LocalRedirect(returnurl);
            }
            
            // Para autenticacion de dos factores:
            if (resultado.RequiresTwoFactor)
            {
                return RedirectToAction("VerificarCodigoAutenticador", new { returnurl = returnurl });
            }
            else
            {
                // Si el usuario no tiene cuenta pregunta si quiere crear una:
                ViewData["ReturnUrl"] = returnurl;
                ViewData["NombreAMostrarProveedor"] = info.ProviderDisplayName;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var nombre = info.Principal.FindFirstValue(ClaimTypes.Name);

                return View("ConfirmacionAccesoExterno",new ConfirmacionAccesoExternoViewModel { Email = email, Nombre = nombre });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ConfirmacionAccesoExterno(ConfirmacionAccesoExternoViewModel confirmacionAE, string? returnurl)
        {
            returnurl = returnurl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("Error");
                }

                var usuario = new UserApp 
                { 
                    UserName = confirmacionAE.Email, 
                    Email = confirmacionAE.Email, 
                    Nombre = confirmacionAE.Nombre 
                };
                var resultado = await _userManager.CreateAsync(usuario);
                if (resultado.Succeeded)
                {
                    resultado = await _userManager.AddLoginAsync(usuario, info);
                    if (resultado.Succeeded)
                    {
                        await _signInManager.SignInAsync(usuario, isPersistent: false);
                        await _signInManager.UpdateExternalAuthenticationTokensAsync(info);
                        return LocalRedirect(returnurl);
                    }
                }

                ValidarErrores(resultado);
            }

            ViewData["ReturnUrl"] = returnurl;
            return View(confirmacionAE);
        }

        // Autenticacion de dos factores:
        [HttpGet]
        public async Task<IActionResult> ActivarAutenticador()
        {
            string formatoUrlAutenticado = "otpauth://totp/{0}:{1}?secret={2}&issuer={0}&digits=6";

            var usuario = await _userManager.GetUserAsync(User);
            await _userManager.ResetAuthenticatorKeyAsync(usuario);
            var token = await _userManager.GetAuthenticatorKeyAsync(usuario);

            // Habilitar codigo QR:
            string urlAutenticador = string.Format(formatoUrlAutenticado, _urlEncoder.Encode("ProyectoIdentity"), _urlEncoder.Encode(usuario.Email), token);

            var adfModel = new AutenticacionDosFactoresViewModel()
            {
                Token = token,
                UrlCodigoQr = urlAutenticador
            };

            return View(adfModel);
        }

        [HttpGet]
        public async Task<IActionResult> EliminarAutenticador()
        {
            var usuario = await _userManager.GetUserAsync(User);
            await _userManager.ResetAuthenticatorKeyAsync(usuario);
            await _userManager.SetTwoFactorEnabledAsync(usuario, false);

            return RedirectToAction(nameof(Index), "Home");
        }

        [HttpPost]
        public async Task<IActionResult> ActivarAutenticador(AutenticacionDosFactoresViewModel autenticacionDF)
        {
            if (ModelState.IsValid)
            {
                var usuario = await _userManager.GetUserAsync(User);
                var succeeded = await _userManager.VerifyTwoFactorTokenAsync(usuario, _userManager.Options.Tokens.AuthenticatorTokenProvider, autenticacionDF.Code);
                if (succeeded)
                {
                    await _userManager.SetTwoFactorEnabledAsync(usuario, true);
                }
                else
                {
                    ModelState.AddModelError("Error", "Su autenticación de dos factores no ha sido validado.");
                    return View(autenticacionDF);
                }
            }

            return RedirectToAction(nameof(ConfirmacionAutenticador));
        }

        [HttpGet]
        public IActionResult ConfirmacionAutenticador()
        {
            return View();
        }
        
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> VerificarCodigoAutenticador(bool recordarDatos, string returnurl)
        {
            var usuario = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (usuario is null)
            {
                return View("Error");
            }

            ViewData["ReturnUrl"] = returnurl;
            return View(new VerificarAutenticadorViewModel { ReturnUrl = returnurl, RecordarDatos = recordarDatos });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> VerificarCodigoAutenticador(VerificarAutenticadorViewModel verificarA)
        {
            verificarA.ReturnUrl = verificarA.ReturnUrl ?? Url.Content("~/");
            if (!ModelState.IsValid)
            {
                return View(verificarA);
            }

            var resultado = await _signInManager.TwoFactorAuthenticatorSignInAsync(verificarA.Code, verificarA.RecordarDatos, rememberClient: true);
            if (resultado.Succeeded)
            {
                return LocalRedirect(verificarA.ReturnUrl);
            }

            if (resultado.IsLockedOut)
            {
                return View("Bloqueado");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Código Inválido");
                return View(verificarA);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Denegado(string? returnurl)
        {
            returnurl = returnurl ?? Url.Content("~/");

            ViewData["ReturnUrl"] = returnurl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ActivarAutenticacion2FEmail()
        {
            var usuario = await _userManager.GetUserAsync(User);
            await _userManager.SetTwoFactorEnabledAsync(usuario, true);

            await _emailSender.SendEmailAsync(usuario.Email, "Autenticación 2F - Activada",
                    $"Se ha activado la autenticación de dos factores en su cuenta correctamente.");

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public async Task<IActionResult> EliminarAutenticacion2FEmail()
        {
            var usuario = await _userManager.GetUserAsync(User);
            await _userManager.SetTwoFactorEnabledAsync(usuario, false);

            await _emailSender.SendEmailAsync(usuario.Email, "Autenticación 2F - Desactivada",
                    $"Se ha desactivado la autenticación de dos factores en su cuenta correctamente.");

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> EnviarCodigoEmail(string email, string returnUrl, bool rememberMe)
        {
            var usuario = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (usuario == null)
            {
                return View("Error");
            }

            var proveedores2F = await _userManager.GetValidTwoFactorProvidersAsync(usuario);
            var proveedoresOpciones = proveedores2F.Select(prov => new SelectListItem { Text = prov, Value = prov }).ToList();
            return View(new EnviarCodigoViewModel
            {
                Email = email,
                Providers = proveedoresOpciones,
                ReturnUrl = returnUrl,
                RememberMe = rememberMe
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> EnviarCodigoEmail(EnviarCodigoViewModel model)
        {
            var usuario = await _userManager.FindByEmailAsync(model.Email);
            var token = await _userManager.GenerateTwoFactorTokenAsync(usuario, model.SelectedProvider);

            await _emailSender.SendEmailAsync(usuario.Email, "Autenticación 2F - Proyecto Identity",
                    $"Su código de verificación es: {token}");

            return RedirectToAction("VerificarCodigo", new { proveedor = model.SelectedProvider, returnUrl = model.ReturnUrl, rememberMe = model.RememberMe });
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult VerificarCodigo(string proveedor, string returnUrl, bool rememberMe)
        {
            return View(new VerificarCodigoViewModel { Provider = proveedor, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> VerificarCodigo(VerificarCodigoViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await _signInManager.TwoFactorSignInAsync(model.Provider, model.Code, model.RememberMe, model.RememberMachine);
            if (result.Succeeded)
            {
                return Redirect(model.ReturnUrl ?? "/");
            }
            else if (result.IsLockedOut)
            {
                return View("Bloqueado");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Código inválido");
                return View(model);
            }
        }
    }
}
