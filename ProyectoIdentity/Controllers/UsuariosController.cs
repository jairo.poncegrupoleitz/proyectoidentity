﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ProyectoIdentity.Claims;
using ProyectoIdentity.Datos;
using ProyectoIdentity.Models;
using System.Security.Claims;
using static ProyectoIdentity.Models.ClaimsUsuarioViewModel;

namespace ProyectoIdentity.Controllers
{
    [Authorize]
    public class UsuariosController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ApplicationDbContext _context;

        public UsuariosController(
            UserManager<IdentityUser> userManager, 
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var usuarios = await _context.UserApp.ToListAsync();
            var rolesUsuario = await _context.UserRoles.ToListAsync();
            var roles = await _context.Roles.ToListAsync();

            foreach (var usuario in usuarios)
            {
                var rol = rolesUsuario.FirstOrDefault(u => u.UserId == usuario.Id);
                if (rol is null)
                {
                    usuario.Rol = "Ninguno";
                }
                else
                {
                    usuario.Rol = roles.FirstOrDefault(u => u.Id == rol.RoleId)!.Name;
                }
            }

            return View(usuarios);
        }

        // Editar usuario (asignacion de rol):
        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public IActionResult Editar(string id)
        {
            var usuarioDb = _context.UserApp.FirstOrDefault(u => u.Id.Equals(id));
            if (usuarioDb is null)
            {
                return NotFound();
            }

            // Obtener los roles actuales del usuario:
            var rolUsuario = _context.UserRoles.ToList();
            var roles = _context.Roles.ToList();
            var rol = rolUsuario.FirstOrDefault(u => u.UserId == usuarioDb.Id);
            if (rol is not null)
            {
                usuarioDb.IdRol = roles.FirstOrDefault(u => u.Id == rol.RoleId)!.Id;
            }
            usuarioDb.ListaRoles = _context.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            });

            return View(usuarioDb);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Editar(UserApp usuario)
        {
            if (ModelState.IsValid)
            {
                var usuarioDb = _context.UserApp.FirstOrDefault(u => u.Id.Equals(usuario.Id));
                if (usuarioDb is null)
                {
                    return NotFound();
                }

                var rolUsuario = _context.UserRoles.FirstOrDefault(u => u.UserId.Equals(usuarioDb.Id));
                if (rolUsuario is not null)
                {
                    // Obtener el rol actual:
                    var rolActual = _context.Roles.Where(u => u.Id.Equals(rolUsuario.RoleId)).Select(e => e.Name).FirstOrDefault();

                    // Eliminar el rol actual:
                    await _userManager.RemoveFromRoleAsync(usuarioDb, rolActual);
                }

                // Agregar usuario al nuevo rol seleccionado:
                await _userManager.AddToRoleAsync(usuarioDb, _context.Roles.FirstOrDefault(u => u.Id == usuario.IdRol)!.Name);
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }

            usuario.ListaRoles = _context.Roles.Select(r => new SelectListItem
            {
                Text = r.Name,
                Value = r.Id
            });

            return View(usuario);
        }

        // Metodo para bloquear/desbloquear usuarios:
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BloquearDesbloquear(string idUsuario)
        {
            var usuarioDb = _context.UserApp.FirstOrDefault(u => u.Id.Equals(idUsuario));
            if (usuarioDb is null)
            {
                return NotFound();
            }

            if (usuarioDb.LockoutEnd != null && usuarioDb.LockoutEnd > DateTime.Now)
            {
                // El usuario se encuentra bloqueado y lo podemos desbloquear:
                usuarioDb.LockoutEnd = DateTime.Now;
                TempData["Correcto"] = "Usuario desbloqueado correctamente";
            }
            else
            {
                // El usuario no esta bloqueado y lo podemos bloquear:
                usuarioDb.LockoutEnd = DateTime.Now.AddYears(100);
                TempData["Error"] = "Usuario bloqueado correctamente";
            }

            _context.SaveChanges();

            return RedirectToAction(nameof(Index));
        }

        // Metodo para borrar usuarios:
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Borrar(string idUsuario)
        {
            var usuarioDb = _context.UserApp.FirstOrDefault(u => u.Id.Equals(idUsuario));
            if (usuarioDb is null)
            {
                return NotFound();
            }

            _context.UserApp.Remove(usuarioDb);

            _context.SaveChanges();

            TempData["Correcto"] = "Usuario borrado correctamente";
            return RedirectToAction(nameof(Index));
        }

        // Editar perfil:
        [HttpGet]
        public IActionResult EditarPerfil(string id)
        {
            if (id is null)
            {
                return NotFound();
            }

            var usuarioBd = _context.UserApp.Find(id);

            if (usuarioBd == null)
            {
                return NotFound();
            }

            return View(usuarioBd);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditarPerfil(UserApp userApp)
        {
            if (ModelState.IsValid)
            {
                var usuario = await _context.UserApp.FindAsync(userApp.Id);
                usuario!.Nombre = userApp.Nombre;
                usuario!.Url = userApp.Url;
                usuario!.CodigoPais = userApp.CodigoPais;
                usuario!.Telefono = userApp.Telefono;
                usuario!.Ciudad = userApp.Ciudad;
                usuario!.Pais = userApp.Pais;
                usuario!.Direccion = userApp.Direccion;
                usuario!.FechaNacimiento = userApp.FechaNacimiento;

                await _userManager.UpdateAsync(usuario);

                return RedirectToAction(nameof(Index), "Home");
            }

            return View(userApp);
        }

        // Cambiar contrasena (con usuario autenticado):
        [HttpGet]
        public IActionResult CambiarPassword(string? id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CambiarPassword(CambiarPasswordViewModel cambiarPass, string email)
        {
            if (ModelState.IsValid)
            {
                var usuario = await _userManager.FindByEmailAsync(email);
                if (usuario == null)
                {
                    return RedirectToAction("Error");
                }

                var token = await _userManager.GeneratePasswordResetTokenAsync(usuario);
                
                var resultado = await _userManager.ResetPasswordAsync(usuario, token, cambiarPass.Password);
                if (resultado.Succeeded)
                {
                    return RedirectToAction("ConfirmacionCambioPassword");
                }
                else
                {
                    return View(cambiarPass);
                }
            }

            return View(cambiarPass);
        }

        [HttpGet]
        public IActionResult ConfirmacionCambioPassword()
        {
            return View();
        }

        // Manejo de claims:
        [HttpGet]
        public async Task<IActionResult> AdministrarClaimsUsuario(string idUsuario)
        {
            IdentityUser usuario = await _userManager.FindByIdAsync(idUsuario);
            if (usuario is null)
            {
                return NotFound();
            }

            var claimUsuarioActual = await _userManager.GetClaimsAsync(usuario);

            var modelo = new ClaimsUsuarioViewModel()
            {
                IdUsuario = idUsuario
            };

            foreach (Claim claim in ManejoClaims.ListaClaims)
            {
                ClaimUsuario claimUsuario = new ClaimUsuario
                {
                    TipoClaim = claim.Type
                };

                if (claimUsuarioActual.Any(c => c.Type == claim.Type))
                {
                    claimUsuario.Seleccionado = true;
                }

                modelo.Claims.Add(claimUsuario);
            }

            return View(modelo);
        }

        [HttpPost]
        public async Task<IActionResult> AdministrarClaimsUsuario(ClaimsUsuarioViewModel claimsUsuario)
        {
            IdentityUser usuario = await _userManager.FindByIdAsync(claimsUsuario.IdUsuario);
            if (usuario is null)
            {
                return NotFound();
            }

            var claims = await _userManager.GetClaimsAsync(usuario);
            var resultado = await _userManager.RemoveClaimsAsync(usuario, claims);

            if (!resultado.Succeeded)
            {
                return View(claimsUsuario);
            }

            resultado = await _userManager.AddClaimsAsync(
                usuario, claimsUsuario.Claims.Where(c => c.Seleccionado)
                .Select(c => new Claim(c.TipoClaim, c.Seleccionado.ToString())));

            if (!resultado.Succeeded)
            {
                return View(claimsUsuario);
            }

            return RedirectToAction(nameof(Index));
        }
    }
}
