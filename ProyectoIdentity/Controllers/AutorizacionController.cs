﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ProyectoIdentity.Controllers
{
    public class AutorizacionController : Controller
    {
        // Acceso libre/publico para el usuario:
        [AllowAnonymous]
        public IActionResult AccesoPublico()
        {
            return View();
        }

        // Solo debe estar autenticado:
        [Authorize]
        public IActionResult AccesoAutenticado()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Usuario"
        //[Authorize(Roles = "Usuario")]
        [Authorize(Policy = "Usuario")]
        public IActionResult AccesoUsuario()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Registrado"
        //[Authorize(Roles = "Registrado")]
        [Authorize(Policy = "Registrado")]
        public IActionResult AccesoRegistrado()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Administrador"
        //[Authorize(Roles = "Administrador")] // opcion 1 con roles
        [Authorize(Policy = "Administrador")] // opcion 2 con policy o directivas
        public IActionResult AccesoAdministrador()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Usuario" o "Administrador"
        [Authorize(Roles = "Usuario, Administrador")]
        public IActionResult AccesoUsuarioAdministrador()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Usuario" y "Administrador"
        [Authorize(Policy = "UsuarioYAdministrador")]
        public IActionResult AccesoUsuarioYAdministrador()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Administrador" y tiene permiso para crear
        [Authorize(Policy = "AdministradorCrear")]
        public IActionResult AccesoAdministradorPermisoCrear()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Administrador" y tiene permiso para editar y borrar
        [Authorize(Policy = "AdministradorEditarBorrar")]
        public IActionResult AccesoAdministradorPermisoEditarBorrar()
        {
            return View();
        }

        // Acceso autenticado y que pertenezca al rol "Administrador" y tiene permiso para crear, editar y borrar
        [Authorize(Policy = "AdministradorCrearEditarBorrar")]
        public IActionResult AccesoAdministradorPermisoCrearEditarBorrar()
        {
            return View();
        }
    }
}
