﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace ProyectoIdentity.Models
{
    public class OlvidoPasswordViewModel
    {
        [Required(ErrorMessage = "El email es obligatorio.")]
        [EmailAddress]
        public string Email { get; set; }
    }
}
