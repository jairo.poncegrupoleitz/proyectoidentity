﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace ProyectoIdentity.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "El email es obligatorio.")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "La contraseña es obligatoria.")]
        [StringLength(50, ErrorMessage = "El {0} debe tener al menos {2} caracteres de longitud.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Required(ErrorMessage = "La confirmación de contraseña es obligatoria.")]
        [Compare("Password", ErrorMessage = "La contraseña y la confirmación de contraseña no coincide.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "El nombre es obligatorio.")]
        public string Nombre { get; set; } = null!;

        public string Url { get; set; } = null!;

        public Int32 CodigoPais { get; set; }

        public string Telefono { get; set; } = null!;

        [Required(ErrorMessage = "El país es obligatorio.")]
        public string Pais { get; set; } = null!;

        public string Ciudad { get; set; } = null!;

        public string Direccion { get; set; } = null!;

        [Required(ErrorMessage = "La fecha de nacimiento es obligatoria.")]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        [Required(ErrorMessage = "El estado es obligatorio.")]
        public bool Estado { get; set; }

        // Para seleccion de roles:
        [Display(Name = "Seleccionar rol")]
        public IEnumerable<SelectListItem>? ListaRoles { get; set; }

        [Display(Name = "Rol seleccionado")]
        public string? RolSeleccionado { get; set; }
    }
}
