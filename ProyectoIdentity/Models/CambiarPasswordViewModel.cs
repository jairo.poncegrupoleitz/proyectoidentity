﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace ProyectoIdentity.Models
{
    public class CambiarPasswordViewModel
    {
        [Required(ErrorMessage = "La contraseña es obligatoria.")]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Required(ErrorMessage = "La confirmación de contraseña es obligatoria.")]
        [Compare("Password", ErrorMessage = "La contraseña y la confirmación de contraseña no coincide.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        public string ConfirmPassword { get; set; }
    }
}
