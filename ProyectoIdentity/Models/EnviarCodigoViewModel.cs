﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace ProyectoIdentity.Models
{
    public class EnviarCodigoViewModel
    {
        public string Email { get; set; }

        public string SelectedProvider { get; set; }

        public ICollection<SelectListItem> Providers { get; set; }

        public string ReturnUrl { get; set; }

        public bool RememberMe { get; set; }
    }
}
