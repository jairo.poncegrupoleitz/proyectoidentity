﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoIdentity.Models
{
    public class AutenticacionDosFactoresViewModel
    {
        // Para el acceso (login)
        [Required]
        [Display(Name = "Codigo del autenticador")]
        public string Code { get; set; }

        // Para el registro:
        public string Token { get; set; }

        // Para codigo QR:
        public string? UrlCodigoQr { get; set; }
    }
}
