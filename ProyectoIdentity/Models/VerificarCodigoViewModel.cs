﻿using System.ComponentModel.DataAnnotations;

namespace ProyectoIdentity.Models
{
    public class VerificarCodigoViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "¿Recordar este navegador?")]
        public bool RememberMachine { get; set; }

        public bool RememberMe { get; set; }
    }
}
