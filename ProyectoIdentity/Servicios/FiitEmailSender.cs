﻿using Fiit.EmailService.Email;
using Fiit.EmailService.Email.Outbound;
using Fiit.EmailService.Outbound;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace ProyectoIdentity.Servicios
{
    public class FiitEmailSender : IEmailSender
    {
        private readonly IEmailService<EmailMessage> _emailServiceProvider;

        public FiitEmailSender(IEmailService<EmailMessage> emailServiceProvider)
        {
            _emailServiceProvider = emailServiceProvider;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var mail = new EmailMessage
            {
                From = new EmailAddress { Name = "Pruebas", Address = "correopruebas920@gmail.com" },
                Body = htmlMessage,
                IsBodyHtml = true,
                Subject = subject,
                To = new[] { new EmailAddress { Address = email } }
            };

            await _emailServiceProvider.SendEmailAsync(mail);
        }
    }
}
