using Fiit.EmailService.Email;
using Fiit.EmailService.Email.Outbound;
using Fiit.EmailService.Outbound;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using ProyectoIdentity.Datos;
using ProyectoIdentity.Servicios;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

// Configuramos la conexion a sql server:
builder.Services.AddDbContext<ApplicationDbContext>(options => 
    options.UseSqlServer(builder.Configuration.GetConnectionString("SqlConnection"))
);

// Agregar el servicio identity a la aplicacion:
builder.Services.AddIdentity<IdentityUser, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

// Esta linea es para la url de retorno al acceder:
builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = new PathString("/Cuentas/Acceso");
    options.AccessDeniedPath = new PathString("/Cuentas/Denegado");
});

// Estas son opciones de configuracion del Identity:
builder.Services.Configure<IdentityOptions>(options =>
{
    options.Password.RequiredLength = 5;
    options.Password.RequireLowercase = true;
    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(1);
    options.Lockout.MaxFailedAccessAttempts = 3;
});

// Agregar autenticacion de facebook:
builder.Services.AddAuthentication().AddFacebook(options =>
{
    options.AppId = "220404704058889";
    options.AppSecret = "e42b1399b8365d0b2e007edcafcd04e7";
});

// Agregar autenticacion de google:
builder.Services.AddAuthentication().AddGoogle(options =>
{
    options.ClientId = "164062237456-3mo9cicrp5l1j5tpto1cju69phmvvu38.apps.googleusercontent.com";
    options.ClientSecret = "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";
});

// Soporte para autorizacion basada en directivas/Policy:
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Administrador", policy => policy.RequireRole("Administrador"));
    options.AddPolicy("Registrado", policy => policy.RequireRole("Registrado"));
    options.AddPolicy("Usuario", policy => policy.RequireRole("Usuario"));

    options.AddPolicy("UsuarioYAdministrador", policy => policy.RequireRole("Usuario").RequireRole("Administrador"));

    // Uso de claims:
    options.AddPolicy("AdministradorCrear", policy => policy.RequireRole("Administrador").RequireClaim("Crear", "True"));
    options.AddPolicy("AdministradorEditarBorrar", policy => policy.RequireRole("Administrador").RequireClaim("Editar", "True").RequireClaim("Borrar", "True"));
    options.AddPolicy("AdministradorCrearEditarBorrar", policy => policy.RequireRole("Administrador").RequireClaim("Crear", "True").RequireClaim("Editar", "True").RequireClaim("Borrar", "True"));
});

// Se agrega IEmailSender:
//builder.Services.AddTransient<IEmailSender, MailJetEmailSender>();
//builder.Services.AddTransient<IEmailSender, SendGridEmailSender>();

builder.Services.AddSingleton<IEmailConfiguration>(builder.Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>());
builder.Services.AddScoped<IEmailService<EmailMessage>, EmailService>();
builder.Services.AddTransient<IEmailSender, FiitEmailSender>();

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

// Se agrega la autenticacion:
app.UseAuthentication();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
